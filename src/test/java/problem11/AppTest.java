package problem11;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.ArrayList;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    public void testGetConvexHull() throws Exception {
        double[][] points = {{1, 2.4}, {2.5, 2}, {1.5, 34.5}, {5.5, 6}, {6, 2.4}, {5.5, 9}};
        assertEquals("Wrong convex hull.", "[(1.0, 2.4), (1.5, 34.5), (5.5, 9.0), (6.0, 2.4), (2.5, 2.0)]",
                App.getConvexHull(points).toString());
    }

    public void testFindRightMostLowestPoint() throws Exception {
        ArrayList<App.MyPoint> points = new ArrayList<>();
        points.add(new App.MyPoint(-2, -3));
        points.add(new App.MyPoint(2, 1));
        points.add(new App.MyPoint(3, 4));
        points.add(new App.MyPoint(0, -3));
        assertEquals("Wrong rightmost lowest point.", "(0.0, -3.0)",
                App.findRightMostLowestPoint(points).toString());
    }

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
