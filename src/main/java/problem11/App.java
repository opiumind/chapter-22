package problem11;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Scanner;

/**
 * (Geometry: Graham’s algorithm for finding a convex hull) Section 22.10.2
 introduced Graham’s algorithm for finding a convex hull for a set of points.
 Assume that the Java’s coordinate system is used for the points.
 Write a test program that prompts the user to enter the set size and the points
 and displays the points that form a convex hull.

 Here is a sample run:
 How many points are in the set? 6
 Enter 6 points: 1 2.4 2.5 2 1.5 34.5 5.5 6 6 2.4 5.5 9
 The convex hull is
 (1.5, 34.5) (5.5, 9.0) (6.0, 2.4) (2.5, 2.0) (1.0, 2.4)
 */
public class App {
    public static ArrayList<MyPoint> getConvexHull(double[][] s) {
        if (s.length < 3) {
            System.out.println("Error. There should be 3 or more points.");
            System.exit(1);
        }
        Deque<MyPoint> convexHullPoints = new ArrayDeque<>();
        ArrayList<MyPoint> allPoints = new ArrayList<>();
        for (int i = 0; i < s.length; i++) {
            allPoints.add(new MyPoint(s[i][0], s[i][1]));
        }

        MyPoint rightMostLowestPoint = findRightMostLowestPoint(allPoints);
        MyPoint.setRightMostLowestPoint(rightMostLowestPoint);
        allPoints.remove(rightMostLowestPoint);

        convexHullPoints.push(rightMostLowestPoint);
//        System.out.println("Lowest: " + convexHullPoints.peekLast());

        allPoints.sort(null);
//        System.out.println("Sorted:");
//        for (MyPoint p : allPoints) {
//            System.out.print(p + " ");
//        }
        convexHullPoints.push(allPoints.get(0));
        convexHullPoints.push(allPoints.get(1));

        int i = 2;
        while (i < allPoints.size()) {
            MyPoint top1 = convexHullPoints.pop();
//            System.out.println("top1: " + top1);
            MyPoint top0 = convexHullPoints.pop();
//            System.out.println("top0: " + top0);
            MyPoint p = allPoints.get(i);
//            System.out.println("p: " + p);
            double x1 = top0.getX();
            double y1 = top0.getY();
            double x2 = top1.getX();
            double y2 = top1.getY();
            double x3 = p.getX();
            double y3 = p.getY();
            convexHullPoints.push(top0);
            convexHullPoints.push(top1);

//            System.out.println("Z: " + ((x3 - x1) * (y2 - y1) - (y3 - y1) * (x2 - x1)));

            // calculate z-coordinate of the cross product of the two vectors
            // if z-coordinate > 0 it means that the point (x3,y3) lies on the right from the line (x1,y1)-(x2,y2)
            if (((x3 - x1) * (y2 - y1) - (y3 - y1) * (x2 - x1)) > 0) {
                convexHullPoints.pop();

            }
            convexHullPoints.push(p);
            i++;
        }

        return new ArrayList<>(convexHullPoints);
    }

    public static MyPoint findRightMostLowestPoint(ArrayList<MyPoint> points) {
        MyPoint rightMostLowestPoint = points.get(0);

        for (int i = 1; i < points.size(); i++) {
            if (points.get(i).getY() < rightMostLowestPoint.getY()) {
                rightMostLowestPoint = points.get(i);
            } else if(points.get(i).getY() == rightMostLowestPoint.getY()) {
                if (points.get(i).getX() > rightMostLowestPoint.getX()) {
                    rightMostLowestPoint = points.get(i);
                }
            }
        }

        return rightMostLowestPoint;
    }

    public static class MyPoint implements Comparable<MyPoint> {
        double x, y;
        Double slope;
        static MyPoint rightMostLowestPoint;

        MyPoint(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public static void setRightMostLowestPoint(MyPoint p) {
            rightMostLowestPoint = p;
        }

        public double getX() {
            return x;
        }

        public double getY() {
            return y;
        }

        public void setSlope() {
            if (x == rightMostLowestPoint.getX()) {
                slope = Double.MAX_VALUE;
            }
            slope = (y - rightMostLowestPoint.getY()) / (x - rightMostLowestPoint.getX());
        }

        @Override
        public int compareTo(MyPoint o) {
            if (slope == null) {
                setSlope();
            }

            if (o.slope == null) {
                o.setSlope();
            }

            // when slopes are positive the bigger slope, the less point
            if (o.slope > 0 && slope > 0) {
                if (o.slope > slope) {
                    return -1;
                } else if (o.slope < slope) {
                    return 1;
                }
                return 0;
            // when both slopes are negative the bigger slope, the bigger point
            } else if (o.slope < 0 && slope < 0) {
                if (o.slope < slope) {
                    return 1;
                } else if (o.slope > slope) {
                    return -1;
                }
                return 0;
            }
            // if only one of slopes are negative, the point with negative slope will be bigger
            if (o.slope < 0) {
                return -1;
            }

            return 1;
        }

        @Override
        public String toString() {
            return "(" + x + ", " + y + ")";
        }
    }

    public static void main(String[] args) {
        ArrayList<MyPoint> convexHullPoints;
        Scanner input = new Scanner(System.in);
        System.out.println("How many points are in the set?");
        int numberOfPoints = input.nextInt();
        double[][] s = new double[numberOfPoints][2];
        System.out.println("Enter " + numberOfPoints + " points' coordinates, divided by space:");

        for (int i = 0; i < numberOfPoints; i++) {
            s[i][0] = input.nextDouble();
            s[i][1] = input.nextDouble();
        }

        convexHullPoints = getConvexHull(s);
        System.out.println("The convex hull is:");
        for (MyPoint p: convexHullPoints) {
            System.out.print(p.toString() + " ");
        }
    }
}
